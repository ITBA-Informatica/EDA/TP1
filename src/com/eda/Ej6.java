package com.eda;

import static com.eda.Utils.*;

public class Ej6 {
    public static long hasSameIdValueTest(int n) {
        int[] testArray = randomSortedArrayWithoutDuplicates(n);
        long startTime = System.currentTimeMillis();
        System.out.println(hasSameIdValue(testArray));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    private static boolean hasSameIdValue(int[] array) {
        for (int i = 0; i < array.length; i++) {

            if (i == array[i]) return true;

            // El valor es mas grande que el indice, como el array es ascendente (sin duplicados) y el
            // indice sube de a uno, es imposible que el indice 'alcance' al valor
            // Si bien esto lo hace mas eficiente, no afecta al orden ya que no afecta al peor caso
            if (array[i] > i) return false;
        }
        return false;
    }
}
