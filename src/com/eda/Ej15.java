package com.eda;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Ej15 {

//    El Ej 15 es muy parecido al 12 o al 13, pero con la diferencia de que cada vez que encuentro
//    una alteracion numerica, en lugar de imprimirla, busco las iteraciones nuevamente de los numeros que la conforman

    public static long printCombinationsTest(int n) {
        long startTime = System.currentTimeMillis();
        printCombinations(n);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static void printCombinations(int n) {
        char[] chars = (String.valueOf(n)).toCharArray();
        int[] numbers = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            numbers[i] = (int) (chars[i] - 48) ;
        }
        printAlterationsRec(numbers, 0,  new char[numbers.length]);
    }

    public static void printAlterationsRec(int[] numbers, int position, char[] output) {
        if (position == numbers.length) {
            System.out.print(String.valueOf(output) + ", ");
            return;
        }

        char[] combinations = possibleChars(numbers[position]);
        for (int i = 0; i < combinations.length; i++) {
            output[position] = combinations[i];
            printAlterationsRec(numbers, position + 1, output);
            if (position == 0) System.out.println();
        }
    }

    public static char[] possibleChars(int n) {
        int base = 97 + 3 * (n-2);
        if (n < 7){
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2)};
            return chars;
        }
        if (n == 7){
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2), (char) (base+3)};
            return chars;
        }
        if (n == 9){
            base += 1;
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2), (char) (base+3)};
            return chars;
        }
        base += 1;
        char[] chars = {(char) (base), (char) (base+1), (char) (base+2)};
        return chars;
    }
}
