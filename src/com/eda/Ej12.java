package com.eda;

public class Ej12 {
    public static long printAlterationsTest(int n) {
        String randomString = Utils.stringWithoutDuplicates(n);
        long startTime = System.currentTimeMillis();
        printAlterations(randomString);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static void printAlterations(String string) {
        printAlterationsRec(string.toCharArray(), 0);
    }

    public static void printAlterationsRec(char[] string, int position) {
        if (position == (string.length - 1)) System.out.println(string);
        for (int i = position; i < string.length; i++) {
            char aux = string[i];
            string[i] = string[position];
            string[position] = aux;

            printAlterationsRec(string, position +1);

            aux = string[i];
            string[i] = string[position];
            string[position] = aux;
        }
    }
}
