package com.eda;

import java.util.*;

import static com.eda.Utils.randomArray;
import static com.eda.Utils.randomSortedArray;

public class Ej11 {
    public static long findIntersectionTest(int n) {
        int[] a = randomSortedArray(n);
        int[] b = randomSortedArray(n);
        long startTime = System.currentTimeMillis();
        int[] intersection = findIntersection(a,b);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(intersection));
        return duration;
    }

    public static int[] findIntersection(int[] a, int[] b) {
        Set<Integer> intersection = new HashSet<>();
        int i = 0;
        int j = 0;
        while (i< a.length && j < b.length) {
            if (a[i] == b[j]) {
                intersection.add(a[i]);
                i++;
                j++;
            } else if (a[i] > b[j]) {
                j++;
            } else {
                i++;
            }
        }

        int[] response = new int[intersection.size()];
        int counter = 0;
        for (Integer in: intersection) {
            response[counter] = in;
            counter++;
        }
        return response;

    }
}
