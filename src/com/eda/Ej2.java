package com.eda;

import static com.eda.Utils.randomArray;

public class Ej2 {
    public static int[] bubbleSort(int[] array){
        int size = array.length;
        boolean changed = true;
        while (changed) {
            changed = false;
            for (int i = 0; i < size - 1; i++) {
                if (array[i + 1] < array[i]) {
                    int aux = array[i];
                    array[i] = array[i+1];
                    array[i+1] = aux;
                    changed = true;
                }
            }
        }
        return array;

    }

    public static long bubbleSortTest(int n) {
        int[] array = randomArray(n);
        long startTime = System.currentTimeMillis();
        array = bubbleSort(array);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }
}
