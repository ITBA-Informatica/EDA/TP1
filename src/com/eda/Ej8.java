package com.eda;

public class Ej8 {
    public static double pow(double base, int exp) {
        if (exp == 0 && base == 0) throw new IllegalArgumentException();
        if (exp < 0) return (1 / powRec(base,-1 * exp));
        return powRec(base, exp);
    }

    public static double powRec(double base, int exp) {
        if (exp == 0) return 1;
        return base * powRec(base, exp -1);
    }

    public static long powTest(int exp) {
        long startTime = System.currentTimeMillis();
        pow(2, exp);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }
}
