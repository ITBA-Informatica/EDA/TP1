package com.eda;

import static com.eda.Utils.randomArray;
import static com.eda.Utils.randomSortedArray;

public class Ej5 {
    public static long hasDuplicatesTest(int n) {
        int[] testArray = randomArray(n);
        long startTime = System.currentTimeMillis();
        hasDuplicates(testArray);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }
    public static boolean hasDuplicates(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if ((array[i] == array[j]) && (i != j)) {
                    System.out.println(i);
                    return true;
                }
            }
        }
        return false;
    }

    public static long hasDuplicatesAscendingTest(int n){
        int[] testArray = randomSortedArray(n);
        long startTime = System.currentTimeMillis();
        hasDuplicatesAscending(testArray);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static boolean hasDuplicatesAscending(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) return true;
        }
        return false;
    }
}
