package com.eda;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.eda.Utils.randomArray;

public class Ej10 {
    public static long findIntersectionTest(int n) {
        int[] a = randomArray(n);
        int[] b = randomArray(n);
        long startTime = System.currentTimeMillis();
        int[] intersection = findIntersection(a,b);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(intersection));
        return duration;
    }

    public static int[] findIntersection(int[] a, int[] b) {
        List<Integer> intersection = new LinkedList<>();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j] && !intersection.contains(a[i])) {
                    ((LinkedList<Integer>) intersection).push(a[i]);
                    break;
                }
            }
        }

        int[] response = new int[intersection.size()];
        int counter = 0;
        for (Integer i: intersection) {
            response[counter] = i;
            counter++;
        }
        return response;

    }
}
