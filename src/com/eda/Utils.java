package com.eda;

public class Utils {


    ////////////////
    //    UTILS
    ////////////////
    public static int[] randomArray(int size) {
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = randomInt();
        }
        return array;
    }

    public static int[] randomPositiveArray(int size) {
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = randomInt(0, 5000);
        }
        return array;
    }



    public static int[] randomSortedArray(int size) {
        int[] array = new int[size];
        int previous = 10;
        for (int i = 0; i < size; i++) {
            array[i] = randomInt(previous, previous+5);
            previous = array[i];
        }
        return array;
    }

    public static int[] randomSortedArrayWithoutDuplicates(int size) {
        int[] array = new int[size];
        int previous = randomInt();
        for (int i = 0; i < size; i++) {
            array[i] = randomInt(previous + 1, previous+1000);
            previous = array[i];
        }
        return array;
    }

    public static int[] randomPositiveSortedArrayWithoutDuplicates(int size) {
        int[] array = new int[size];
        int previous = randomInt();
        for (int i = 0; i < size; i++) {
            array[i] = randomInt(previous + 1, previous+1000);
            previous = array[i];
        }
        return array;
    }

    public static int randomInt(int min, int max) {
        return (int) Math.floor(Math.random() * (max-min+1) + min);
    }

    public static int randomInt() {
        return randomInt(-5000, 5000);
    }

    public static String stringWithoutDuplicates(int size ) {
        if (size > 58) return stringWithoutDuplicates(58);
        char[] string= new char[size];
        for (int i = 0; i < size; i++) {
            string[i] = (char) (i +65);
        }
        return String.valueOf(string);
    }

    public static String stringWithDuplicates(int size ) {
        if (size > 58) return stringWithoutDuplicates(58);
        char[] string= new char[size];
        for (int i = 0; i < size; i++) {
            int charCode = randomInt(0, 5);
            string[i] = (char) (charCode +65);
        }
        return String.valueOf(string);
    }

    public static void printBooleanMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
//                System.out.print(matrix[i][j]);
                if (matrix[i][j] != 0) {
                    System.out.print((char) 9633);
                } else {
                    System.out.print((char) 9632);
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }

}
