package com.eda;

import static com.eda.Utils.*;

public class Ej7 {
    public static long hasSameIdValuePositivesTest(int n) {
        int[] testArray = randomPositiveSortedArrayWithoutDuplicates(n);
        long startTime = System.currentTimeMillis();
        hasSameIdValuePositives(testArray);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    private static boolean hasSameIdValuePositives(int[] array) {

        // Este metodo directamente se fija si el primer valor es 0, caso contrario es imposible que el indice 'alcance'
        // a los valores, ya que ambos son crecientes, y el indice crece de a uno (orden cnostante)
        return array[0] == 0;
    }

    private static boolean hasSameIdValuePositivesB(int[] array) {
        for (int i = 0; i < array.length; i++) {

            if (i == array[i]) return true;

            // El valor es mas grande que el indice, como el array es ascendente (sin duplicados) y el
            // indice sube de a uno, es imposible que el indice 'alcance' al valor
            // Si bien esto lo hace mas eficiente, no afecta al orden ya que no afecta al peor caso
            if (array[i] > i) return false;
        }
        return false;
    }

}