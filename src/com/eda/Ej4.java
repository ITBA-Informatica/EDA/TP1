package com.eda;

import static com.eda.Utils.randomArray;
import static com.eda.Utils.randomSortedArray;

public class Ej4 {
    public static long isAscendingBest(int n)  {
        return isAscendingTest(randomArray(n));
    }

    public static long isAscendingWorst(int n)  {
        return isAscendingTest(randomSortedArray(n));
    }

    public static long isAscendingTest(int[] array){
        long startTime = System.currentTimeMillis();
        isAscending(array);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static boolean isAscending(int[] array) {
        if (array.length <= 1) return true;
        int previous = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < previous) return false;
            previous = array[i];
        }
        return true;
    }
}
