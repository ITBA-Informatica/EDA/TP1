package com.eda;

import java.util.LinkedList;
import java.util.List;

public class Ej13 {
    public static long printAlterationsTest(int n) {
        String randomString = Utils.stringWithDuplicates(n);
        System.out.println("Test String: " + randomString);
        long startTime = System.currentTimeMillis();
        printAlterations(randomString);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static void printAlterations(String string) {
        printAlterationsRec(string.toCharArray(), 0);
    }

    public static void printAlterationsRec(char[] string, int position) {
        if (position == string.length - 1) {
            System.out.println(string);
            return;
        }
        List<Character> switches = new LinkedList<>();
        for (int i = position; i < string.length; i++) {
            if (!switches.contains(string[i])) {
                char aux = string[i];
                string[i] = string[position];
                string[position] = aux;

                printAlterationsRec(string, position + 1);

                aux = string[i];
                string[i] = string[position];
                string[position] = aux;
                switches.add(string[i]);
            }
        }
    }
}
