package com.eda;

import javax.sound.midi.Soundbank;
import java.util.Arrays;

import static com.eda.Ej14.paintBooleanMatrix;
import static com.eda.Ej14.paintBooleanMatrixTest;
import static com.eda.Ej15.possibleChars;
import static com.eda.Ej15.printCombinationsTest;
import static com.eda.Utils.*;

public class Main {

    public static void main(String[] args) {
        printCombinationsTest(256);
    }
}
