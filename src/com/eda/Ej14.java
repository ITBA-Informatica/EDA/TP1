package com.eda;

import static com.eda.Utils.printBooleanMatrix;

public class Ej14 {
    public static long paintBooleanMatrixTest(int x, int y, int toPaintValue) {

//        Vamos a pintar la matriz del ejemplo para que sea bien claro

        int[][] booleanMatrix = createExampleMatrix();
        paintBooleanMatrix(booleanMatrix, x,y, toPaintValue);
        printBooleanMatrix(booleanMatrix);
        long startTime = System.currentTimeMillis();

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static int[][] createExampleMatrix() {
        int[][] bm = new int[12][12];
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 12; j++) {
                bm[i][j] = 0;
            }
        }

        bm[0][4] = 1;
        bm[0][5] = 1;
        bm[0][6] = 1;
        bm[0][7] = 1;
        bm[1][2] = 1;
        bm[1][3] = 1;
        bm[1][8] = 1;
        bm[1][9] = 1;
        bm[2][1] = 1;
        bm[2][10] = 1;
        bm[3][0] = 1;
        bm[3][11] = 1;
        bm[4][0] = 1;
        bm[4][11] = 1;
        bm[5][0] = 1;
        bm[5][11] = 1;
        bm[6][0] = 1;
        bm[6][11] = 1;
        bm[7][0] = 1;
        bm[7][11] = 1;
        bm[8][0] = 1;
        bm[8][11] = 1;
        bm[11][4] = 1;
        bm[11][5] = 1;
        bm[11][6] = 1;
        bm[11][7] = 1;
        bm[10][2] = 1;
        bm[10][3] = 1;
        bm[10][8] = 1;
        bm[10][9] = 1;
        bm[9][1] = 1;
        bm[9][10] = 1;
        return bm;
    }

    public static void paintBooleanMatrix(int[][] bm, int x, int y, int toPaintValue) {
        if (bm[x][y] == toPaintValue) return;
        bm[x][y] = toPaintValue;
        if (x != 0) {
            paintBooleanMatrix(bm, x-1,y, toPaintValue);
        }
        if (y != 0) {
            paintBooleanMatrix(bm, x,y-1, toPaintValue);
        }
        if (x != (bm.length - 1)) {
            paintBooleanMatrix(bm, x+1,y, toPaintValue);
        }
        if (y != (bm[0].length - 1)) {
            paintBooleanMatrix(bm, x,y+1, toPaintValue);
        }
    }
}
