# EDA - TP1

## Usage:

Each method comes with it's own tester method, usually requiring the input size as parameter. All tester methods will return the time (in ms) it took to run the algorythm.

## Todo:

- Wrappers functions that run each tester method multiple times and output a list of (input, time) pair values.

- Ej 15